'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');
var _ = require('lodash');

var dasherize = function(str) {
  if (!str) {
    return '';
  }
  return str.replace(/([^-A-Z])([A-Z]+)/g, function(arg$, lower, upper) {
    return lower + '-' + (upper.length > 1 ? upper : upper.toLowerCase());
  }).replace(/^([A-Z]+)/, function(arg$, upper) {
    if (upper.length > 1) {
      return upper + '-';
    } else {
      return upper.toLowerCase();
    }
  });
};
var lower = function(str) {
  if (str) {
    return str.toLowerCase();
  }
  return '';
};

_.dasherize = dasherize;
_.lower = lower;

module.exports = yeoman.Base.extend({
  prompting: function() {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to ' + chalk.red('generator-ownangular') + ' generator!'
    ));

    var prompts = [{
      type: 'list',
      name: 'moduleType',
      message: 'What type of element would you like to create?',
      choices: [
        'directive',
        'entity',
        'filter',
        'service',
        'page'
      ],
      default: 'page'
    }, {
      name: 'moduleName',
      message: 'Name of the MODULE to use',
      default: this.appname
    }, {
      when: function(response) {
        return response.moduleType == 'directive';
      },
      name: 'directiveName',
      message: 'Name of the DIRECTIVE to create'
    }, {
      when: function(response) {
        return response.moduleType == 'entity';
      },
      name: 'entityName',
      message: 'Name of the ENTITY to create'
    }, {
      when: function(response) {
        return response.moduleType == 'filter';
      },
      name: 'filterName',
      message: 'Name of the FILTER to create'
    }, {
      when: function(response) {
        return response.moduleType == 'service';
      },
      name: 'serviceName',
      message: 'Name of the SERVICE to create'
    }, {
      when: function(response) {
        return response.moduleType == 'page';
      },
      name: 'pageName',
      message: 'Name of the PAGE to create'
    }, {
      when: function(response) {
        return response.moduleType == 'directive';
      },
      type: 'list',
      name: 'directiveType',
      message: 'Choise a TYPE for the directive',
      choices: [
        'E', 'A'
      ],
      default: 'E'
    }, {
      when: function(response) {
        return response.moduleType == 'directive';
      },
      name: 'moduleHTML',
      message: 'Use HTML file?',
      type: 'confirm',
      default: true
    }, {
      when: function(response) {
        return response.moduleType == 'page';
      },
      name: 'pageUrl',
      message: 'STATE of the page (/name)',
      default: function(answers) {
        return _.lower(answers.pageName);
      }
    }, {
      when: function(response) {
        return ['page', 'directive'].indexOf(response.moduleType) > -1;
      },
      name: 'controllerName',
      message: 'Name of the CONTROLLER to use',
      default: function(answers) {
        return answers.pageName || answers.directiveName;
      }
    }, {
      when: function(response) {
        return ['page', 'directive'].indexOf(response.moduleType) > -1;
      },
      name: 'moduleSCSS',
      message: 'Use scss?',
      type: 'confirm',
      default: true
    }, {
      when: function(response) {
        return ['page', 'directive'].indexOf(response.moduleType) > -1;
      },
      name: 'moduleImage',
      message: 'Use images?',
      type: 'confirm',
      default: false
    }, {
      when: function(response) {
        return ['page', 'directive'].indexOf(response.moduleType) > -1;
      },
      name: 'moduleTranslate',
      message: 'Use translate files?',
      type: 'confirm',
      default: true
    }];

    return this.prompt(prompts).then(function(props) {
      // To access props later use this.props.someAnswer;
      this.props = props;
      var parse = [
        'moduleName',
        'directiveName',
        'entityName',
        'filterName',
        'serviceName',
        'pageName',
        'pageUrl',
        'controllerName',
      ];
      var firstcase = {
        'entityName': true,
        'filterName': true,
        'serviceName': true,
        'controllerName': true,
      }
      for (var i = 0; i < parse.length; i++) {
        var p = parse[i];
        if (this.props[p]) {
          this.props[p] = _.camelCase(this.props[p]);
          if (firstcase.hasOwnProperty(p) && firstcase[p]) {
            this.props[p] = this.props[p].charAt(0).toUpperCase() + this.props[p].slice(1);
          }
        }
      }
      this.props._ = _;
    }.bind(this));
  },

  writing: function() {
    switch (this.props.moduleType) {
      case 'directive':
        copyDirective(this);
        break;
      case 'entity':
        copyEntity(this);
        break;
      case 'filter':
        copyFilter(this);
        break;
      case 'service':
        copyService(this);
        break;
      case 'page':
        copyPage(this);
        break;
      default:
    }
    copyOptionals(this);
  },

  install: function() {
    return;
    // this.installDependencies();
  }
});

function parseFile(file) {
  return _.lower(_.camelCase(file));
}

function copyPage(self) {
  var filename = parseFile(self.props.pageName);
  var path = 'app/scripts/pages/' + filename;
  self.fs.copyTpl(
    self.templatePath('pages/pagename/_.route.js'),
    self.destinationPath(path + '/' + filename + '.route.js'),
    self.props
  );
  self.fs.copyTpl(
    self.templatePath('pages/pagename/_.page.html'),
    self.destinationPath(path + '/' + filename + '.page.html'),
    self.props
  );
  self.fs.copyTpl(
    self.templatePath('common/_.ctrl.js'),
    self.destinationPath(path + '/' + filename + '.ctrl.js'),
    self.props
  );
}

function copyService(self) {
  var filename = parseFile(self.props.serviceName);
  var path = 'app/scripts/generic/services';
  self.fs.copyTpl(
    self.templatePath('generic/services/_.service.js'),
    self.destinationPath(path + '/' + filename + '.service.js'),
    self.props
  );
}

function copyFilter(self) {
  var filename = parseFile(self.props.filterName);
  var path = 'app/scripts/generic/filters';
  self.fs.copyTpl(
    self.templatePath('generic/filters/_.filter.js'),
    self.destinationPath(path + '/' + filename + '.filter.js'),
    self.props
  );
}

function copyEntity(self) {
  var filename = parseFile(self.props.entityName);
  var path = 'app/scripts/generic/entities';
  self.fs.copyTpl(
    self.templatePath('generic/entities/_.entity.js'),
    self.destinationPath(path + '/' + filename + '.entity.js'),
    self.props
  );
}

function copyDirective(self) {
  var filename = parseFile(self.props.directiveName);
  var path = 'app/scripts/generic/directives/' + filename;
  self.fs.copyTpl(
    self.templatePath('generic/directives/_.directive.js'),
    self.destinationPath(path + '/' + filename + '.directive.js'),
    self.props
  );
  self.fs.copyTpl(
    self.templatePath('common/_.ctrl.js'),
    self.destinationPath(path + '/' + filename + '.ctrl.js'),
    self.props
  );
  if (self.props.moduleHTML) {
    self.fs.copyTpl(
      self.templatePath('generic/directives/_directive.tpl.html'),
      self.destinationPath(path + '/' + filename + '.tpl.html'),
      self.props
    );
  }
}

function copyOptionals(self) {
  var filename = '';
  var path = '';
  switch (self.props.moduleType) {
    case 'directive':
      filename = parseFile(self.props.directiveName);
      path = 'app/scripts/generic/directives/' + filename;
      break;
    case 'page':
      filename = parseFile(self.props.pageName);
      path = 'app/scripts/pages/' + filename;
      break;
    default:
  }
  if (self.props.moduleSCSS) {
    self.fs.copy(
      self.templatePath('common/__style.scss'),
      self.destinationPath(path + '/_' + filename + '.scss')
    );
  }
  if (self.props.moduleImage) {
    self.fs.copy(
      self.templatePath('common/images/.deleteme'),
      self.destinationPath(path + '/images/.deleteme')
    );
  }
  if (self.props.moduleTranslate) {
    self.fs.copy(
      self.templatePath('common/_locale-lang.json'),
      self.destinationPath(path + '/locale-es.json')
    );
    self.fs.copy(
      self.templatePath('common/_locale-lang.json'),
      self.destinationPath(path + '/locale-ca.json')
    );
  }
}
