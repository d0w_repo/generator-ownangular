(function() {
  'use strict';

  angular.module('<%= moduleName %>')
    .service('<%= serviceName %>', ['$log',
      function($log) {
        $log.debug('<%= moduleName %>:<%= serviceName %> loaded SERVICE');
        var <%= _.lower(serviceName) %> = {};

        return <%= _.lower(serviceName) %>;
      }
    ]);
}());
