(function() {
  'use strict';

  angular.module('<%= moduleName %>')
    .config(['$stateProvider', function($stateProvider) {
      $stateProvider
        .state('<%= _.lower(pageName) %>', {
          url: '/<%= pageUrl %>',
          templateUrl: 'scripts/pages/<%= _.lower(pageName) %>/<%= _.lower(pageName) %>.page.html',
          controller: '<%= controllerName %>',
          controllerAs: '<%= _.lower(controllerName) %>'
        });
    }]);

})();
