(function() {
  'use strict';

  angular.module('<%= moduleName %>')
    .factory('<%= _.capitalize(entityName) %>', [function() {
      function <%= _.capitalize(entityName) %>(obj) {
        var self = this;

        self.id = null;

        self.setObject = function(obj) {
          if (typeof obj !== 'object') {
            obj = {};
          }
          self.id = obj.id || null;
        };

        self.isValid = function() {
          return true;
        };

        self.getObject = function() {
          return {
            id: self.id
          };
        };

        self.setObject(obj);
      }

      return <%= _.capitalize(entityName) %>;
    }]);
}());
