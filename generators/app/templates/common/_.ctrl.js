(function() {
  'use strict';

  angular.module('<%= moduleName %>')
    .controller('<%= controllerName %>', ['$log', function($log) {
      $log.debug('<%= moduleName %>:<%= controllerName %> loaded CONTROLLER');
      var <%= _.lower(controllerName) %> = this;

    }]);
})();
