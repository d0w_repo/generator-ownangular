(function() {
  'use strict';

  angular.module('<%= moduleName %>')
    .directive('<%= directiveName %>', function() {
      return {
        restrict: '<%= directiveType %>',
        scope: {},
        replace: true,
        transclude: false,
        link: function link(scope, elem, attr, $log) {
          $log.debug('<%= moduleName %>:<%= directiveName %> loaded - <<%= _.dasherize(directiveName) %>></<%= _.dasherize(directiveName) %>>');
        },
        templateUrl: 'scripts/generic/directives/<%= _.lower(directiveName) %>/<%= _.lower(directiveName) %>.tpl.html',
        controller: '<%= controllerName %>',
        controllerAs: '<%= _.lower(controllerName) %>'
      };
    });
}());
